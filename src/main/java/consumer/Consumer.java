package consumer;

import com.google.gson.Gson;
import com.rabbitmq.client.*;
import common.Dvd;
import consumer.services.MailService;

import java.io.FileWriter;
import java.io.IOException;

public class Consumer {
    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        final MailService mailService = new MailService("dummymail@gmail.com", "dummypassword");
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                Gson gson = new Gson();
                Dvd dvd = gson.fromJson(message, Dvd.class);
                System.out.println(" [x] Received '" + dvd.toString() + "'");
                writeFile(dvd);
                mailService.sendMail("dummymail@gmail.com", "Dummy Mail", dvd.toString());

            }
        };
        channel.basicConsume(queueName, true, consumer);
    }

    public static void writeFile(Dvd dvd) throws IOException {
        FileWriter writer = new FileWriter("movies.txt", true);
        writer.write("\r\nNew dvd added: \r\n");
        writer.write(dvd.toString());
        writer.close();
    }
}
