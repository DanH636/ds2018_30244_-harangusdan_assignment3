package producer;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addServlet")
public class Producer extends HttpServlet {
    private static final String EXCHANGE_NAME = "logs";

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);

            resp.setContentType("text/html");

            String title = req.getParameter("title");
            int year = Integer.parseInt(req.getParameter("year"));
            int price = Integer.parseInt(req.getParameter("price"));

            String dvd = "{'title' : " + "'" + title + "'" + ",'year' : " + year + ",'price' : " + price + "}";

            channel.basicPublish(EXCHANGE_NAME, "", null, dvd.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + dvd + "'");

            channel.close();
            connection.close();

            resp.sendRedirect("/index.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
