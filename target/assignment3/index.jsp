<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>DVD store admin</title>
    <style type="text/css">
        .paddingBtm {
            padding-bottom: 12px;
        }
    </style>
</head>
<body>
<h2>DVD store admin</h2>
<form id="addDvdForm" name="addForm" method="post" action="addServlet">
    <div id="titleDiv" class="paddingBtm">
        <span>Title: </span><input type="text" name="title" />
    </div>
    <div id="yearDiv" class="paddingBtm">
        <span>Year: </span><input type="text" name="year" />
    </div>
    <div id="priceDiv" class="paddingBtm">
        <span>Price: </span><input type="text" name="price" />
    </div>
    <div id="addBtn">
        <input id="btn" type="submit" value="Add DVD" />
    </div>
</form>
</body>
</html>